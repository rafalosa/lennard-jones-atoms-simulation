import numpy as np
from scipy import constants as const
import matplotlib.pyplot as plt
import math
import matplotlib.animation as anim
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.widgets import Slider,Button
from matplotlib.pyplot import figure

class ButtonOperator:

    def Start(self,event):

        global StartCondition
        StartCondition = True;

    def Reset(self,event):

        global Objs,StartCondition,N,InitialEnergy,Pos,Vel,KineticText,PotentialText,NetText,TempText,InitialText,Time,Iteration,TimeText

        Time = 0
        Iteration = 0
        StartCondition = False
        Objs = RandomCluster(N)
        Pos = GeneratePositionsArray(Objs)
        Vel = GenerateVelocitiesArray(Objs)
        InitialEnergy = CalculateNetPotentialEnergy(Objs) + CalculateNetKineticEnergy(Objs)
        AnimatedGraph._offsets3d = (Pos[:,0],Pos[:,1],Pos[:,2])

        NetPot = CalculateNetPotentialEnergy(Objs)
        NetKin = CalculateNetKineticEnergy(Objs)

        InitialText.remove()
        KineticText.remove()
        PotentialText.remove()
        NetText.remove()
        TempText.remove()
        TimeText.remove()

        InitialText = plt.gcf().text(0.7, 0.70, 'Initial Energy = {:.2E}'.format(InitialEnergy), fontsize=11)
        KineticText = plt.gcf().text(0.7, 0.65, 'Current kinetic = {:.4E}'.format(NetKin), fontsize = 11)
        PotentialText = plt.gcf().text(0.7, 0.60, 'Current Potential= {:.4E}'.format(NetPot), fontsize = 11)
        NetText = plt.gcf().text(0.7, 0.55, 'Current total = {:.4E}'.format(NetKin + NetPot), fontsize = 11)
        TempText = plt.gcf().text(0.7,0.50,'Current temperature = {:.2f}'.format(2*NetKin/(N*3*const.k)),fontsize = 11) # Class that handles buttons
        TimeText = plt.gcf().text(0.4,0.9,'Time = {:.2E} s'.format(Time),fontsize = 14)

class Particle: # Class that describes a single particle, contains it's mass, position and velocity vectors

    def __init__(self,P,V,M):

        self.position = P
        self.velocity = V
        self.mass = M

    def ForceBetween(self,other): # Method that calculates the force exerted by two particles on each other

        global varEpsilon, sigma

        ForceVector = np.zeros((1,3))
        Distance = np.linalg.norm(self.position - other.position) # Distance between interacting particles

        if Distance <= 40 * sigma: # If particles are over 40 sigma apart, then the interaction is ignored

            for i in range(0,3):

                    ForceVector[0,i] = -48 * varEpsilon *(((sigma/Distance)**12)/Distance - ((sigma/Distance)**6)/(2*Distance))\
                    * (other.position[i] - self.position[i])/Distance # The main physics of the simulation - force derived from Lennard - Jones potential

        return ForceVector

    def CalculateLJPotential(self,other): # Function used to determine the potential energy at current time step, between two particles

        global sigma,varEpsilon

        Potential = 0

        Distance = np.linalg.norm(self.position - other.position)

        Potential = 4*varEpsilon*((sigma/Distance)**12 - (sigma/Distance)**6)

        return Potential

############################## Functions that create various particles configurations ##################################

def Cube(Distance):

    ObjList = []

    ObjList.append(Particle(np.array([-Distance,-Distance,Distance]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([Distance,Distance,Distance]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([Distance,-Distance,Distance]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([-Distance,Distance,Distance]),np.array([0,0,0]),const.electron_mass*1000*19))

    ObjList.append(Particle(np.array([-Distance,-Distance,-Distance]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([Distance,Distance,-Distance]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([Distance,-Distance,-Distance]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([-Distance,Distance,-Distance]),np.array([0,0,0]),const.electron_mass*1000*19))

    return ObjList

def Tetraedr(Distance):

    ObjList = []

    Height = Distance*np.sqrt(3)/2

    ObjList.append(Particle(np.array([-Distance/2,-Height/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([Distance/2,-Height/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([0,Height*2/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([0,0,Distance*np.sqrt(6)/3]),np.array([0,0,0]),const.electron_mass*1000*19))

    return ObjList

def Triangle(Distance):

    ObjList = []

    Height = Distance*np.sqrt(3)/2

    ObjList.append(Particle(np.array([-Distance/2,-Height/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([Distance/2,-Height/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([0,Height,0]),np.array([0,0,0]),const.electron_mass*1000*19))

    return ObjList

def EquilateralTriangle(Distance):

    ObjList = []

    Height = Distance*np.sqrt(3)/2

    ObjList.append(Particle(np.array([-Distance/2,-Height/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([Distance/2,-Height/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))
    ObjList.append(Particle(np.array([0,Height*2/3,0]),np.array([0,0,0]),const.electron_mass*1000*19))

    return ObjList

########################################################################################################################

def CalculateNetPotentialEnergy(Objs):

    NetPotential = 0

    for i in range(0,len(Objs)):
        for j in range(i+1,len(Objs)):

            NetPotential += Objs[i].CalculateLJPotential(Objs[j])

    return NetPotential # Function that calculates the net potential energy

def CalculateNetKineticEnergy(Objs):

    NetKineticEnergy = 0

    for Particle in Objs:
        for i in range(0,3):

            NetKineticEnergy += 0.5*Particle.mass * Particle.velocity[i]**2

    return NetKineticEnergy # Function that calculates the net kinetic energy

def CalculateAccelerationArray(Objects = []): # Calculating the main acceleration matrix

    Acceleration = np.zeros((len(Objects),3))

    for i in range(0,len(Objects)):
            for j in range(0,len(Objects)):

                if i != j:

                    Acceleration[i] = Acceleration[i] + Objects[i].ForceBetween(Objects[j])/Objects[i].mass

    return Acceleration

def GeneratePositionsArray(Objects = []): # Creating the main positions matrix

    Positions = np.zeros((len(Objects),3))

    for i in range(len(Objects)):

        Positions[i] = Objects[i].position

    return Positions

def GenerateVelocitiesArray(Objects = []): # Creating the main velocities matrix

    Velocities = np.zeros((len(Objects),3))

    for i in range(len(Objects)):

        Velocities[i] = Objects[i].velocity

    return Velocities

def RandomCluster(N): # Function that generates N particles in random locations with velocity=0

    global sigma
    RandPos = lambda: (np.random.rand()*6-3)*sigma
    ObjList = []
    Check = False

    for i in range(N):

        Check = False

        while(not Check):

            Part = Particle(np.array([RandPos(),RandPos(),RandPos()]) , np.array([0,0,0]) , const.electron_mass*1020*16) #Tworzenie obiektu cząstki

            if(all(np.linalg.norm(Obj.position - Part.position) >= sigma for Obj in ObjList)):
                ObjList.append(Part)
                Check = True

    return ObjList

def AdjustTimeStep(AccMatrix, VelMatrix,Fraction): # Dynamic time step adjusting to minimize error

    global N,sigma

    min = 1

    for i in range(N):
        for j in range(3):

            dt1 = (-2*abs(VelMatrix[i,j]) + np.sqrt(4*VelMatrix[i,j]**2 + 4*abs(AccMatrix[i,j])*sigma*2*Fraction/np.sqrt(3)))/2/abs(AccMatrix[i,j] + 1)
            dt2 = (-2*abs(VelMatrix[i,j]) - np.sqrt(4*VelMatrix[i,j]**2 + 4*abs(AccMatrix[i,j])*sigma*2*Fraction/np.sqrt(3)))/2/abs(AccMatrix[i,j] + 1)

            if(dt1 < 0):

                Candidate = dt2

            else:

                Candidate = dt1

            if(min > Candidate):

                min = Candidate
    return min

def Simulate(frame): # Main simulation loop

    global Pos,Vel,dt,Objs,KineticText,PotentialText,NetText,TempText,N,StartCondition,Title,Iteration,Time,TimeText,sigma

    if(StartCondition):

        Accel = CalculateAccelerationArray(Objs)

        dt = AdjustTimeStep(Accel,Vel,0.05) # Function that adjusts time step based on current velocities and accelerations arrays
                                             #Third parameter determines the max. sigma fraction particles can move during single time step

        dr = Vel * dt + 0.5*Accel*dt**2
        Pos += dr  # Euler method of solving differential equations

        Vel += Accel * dt

        Time += dt # Tracking simulation time

        n = 0

        for Particle in Objs: # Updating particles' parameters

            Particle.position = Pos[n]
            Particle.velocity = Vel[n]

            n += 1

        if(Iteration == 15): # Visual updating data every 15 cycles

            NetPot = CalculateNetPotentialEnergy(Objs)
            NetKin = CalculateNetKineticEnergy(Objs)

            KineticText.remove()
            PotentialText.remove()
            NetText.remove()
            TempText.remove()

            KineticText = plt.gcf().text(0.7, 0.65, 'Current kinetic = {:.4E} J'.format(NetKin), fontsize = 11)
            PotentialText = plt.gcf().text(0.7, 0.60, 'Current Potential= {:.4E} J'.format(NetPot), fontsize = 11)
            NetText = plt.gcf().text(0.7, 0.55, 'Current total = {:.4E} J'.format(NetKin + NetPot), fontsize = 11)
            TempText = plt.gcf().text(0.7,0.50,'Current temperature = {:.2f} K'.format(2*NetKin/(N*3*const.k)),fontsize = 11)
            Iteration = 0


        Iteration += 1
        Title.remove()
        TimeText.remove()
        TimeText = plt.gcf().text(0.4,0.9,'Time = {:.2E} s'.format(Time),fontsize = 14)
        Title = plt.gcf().text(0.7,0.75,'ε = {:.2E}   σ = {:.2E}   N ={}'.format(varEpsilon,sigma,N))


        AnimatedGraph._offsets3d = (Pos[:,0],Pos[:,1],Pos[:,2])

def TwoParticles(Distance):

    ObjList = []

    ObjList.append(Particle(np.array([-Distance/2,0,0]),np.array([0,0,0]),const.electron_mass*1000*20))
    ObjList.append(Particle(np.array([Distance/2,0,0]),np.array([0,0,0]),const.electron_mass*1000*20))

    return ObjList

def SlidersUpdate(val):

    global varEpsilon, sigma

    varEpsilon = EpsSlider.val
    sigma = SigmaSlider.val

dt = 0 # Setting time step
varEpsilon = 1.65e-20 # Parameter of force
sigma = 3.4e-10 # Parameter of the zero-potential distance
N = 20 # Number of particles

Iteration = 0
Time = 0

StartCondition = False # Variable that handles the start of the animation

Objs = RandomCluster(N) # Choosing the particle generating function
InitialEnergy = CalculateNetPotentialEnergy(Objs) + CalculateNetKineticEnergy(Objs) # Calculating the starting potential energy

Pos = GeneratePositionsArray(Objs)
Vel = GenerateVelocitiesArray(Objs)

fig = plt.figure(num=None, figsize=(10, 11), dpi=80)

ax = fig.add_subplot(111,projection = '3d',autoscale_on = False,xlim = [-1.5e-9,1.5e-9],ylim = [-1.5e-9,1.5e-9],zlim = [-1.5e-9,1.5e-9])
plt.subplots_adjust(right = 0.7,bottom = 0.5)

Title = plt.gcf().text(0.7,0.75,'ε = {:.2E}   σ = {:.2E}   N ={}'.format(varEpsilon,sigma,N))
InitialText = plt.gcf().text(0.7, 0.70, 'Initial Energy = {:.2E} J'.format(InitialEnergy), fontsize=11)
KineticText = plt.gcf().text(0.7, 0.65, 'Current Kinetic = {:.2E} J'.format(CalculateNetKineticEnergy(Objs)), fontsize=11)
PotentialText = plt.gcf().text(0.7, 0.60, 'Current Potential= {:.2E} J'.format(InitialEnergy), fontsize=11)
NetText = plt.gcf().text(0.7, 0.55, 'Current Total = {:.2E} J'.format(InitialEnergy), fontsize=11)
TempText = plt.gcf().text(0.7,0.50,'Current temperature = {} K'.format(2*CalculateNetKineticEnergy(Objs)/(3*N*const.k)))
TimeText = plt.gcf().text(0.4,0.9,'Time = {:.2E} s'.format(Time),fontsize = 14)

EpsSliderArea = plt.axes([0.1, 0.2, 0.8, 0.02])
EpsSlider = Slider(EpsSliderArea, 'ε',valmin=varEpsilon/2,valmax=varEpsilon*2,valinit = varEpsilon)

SigmaSliderArea = plt.axes([0.1,0.3,0.8,0.02])
SigmaSlider = Slider(SigmaSliderArea, 'σ',valmin=1e-10,valmax=1e-9,valinit = sigma)

ResetArea = plt.axes([0.3-0.045, 0.1, 0.09, 0.06])
ResetButton = Button(ResetArea,'RESET')

StartArea = plt.axes([0.7-0.06-0.045,0.1,0.09,0.06])
StartButton = Button(StartArea,'START')

callback = ButtonOperator()

EpsSlider.on_changed(SlidersUpdate)
SigmaSlider.on_changed(SlidersUpdate)
#TimeSlider.on_changed(SlidersUpdate)

StartButton.on_clicked(callback.Start)
ResetButton.on_clicked(callback.Reset)

AnimatedGraph = ax.scatter(Pos[:,0],Pos[:,1],Pos[:,2])
ax.grid(b = None)

Animation = anim.FuncAnimation(fig,Simulate,10000,interval = 1)

#Animation.save('{}_particles.mp4'.format(N), writer=writer)

plt.show()
